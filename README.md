# CS216-String Matching Project

## Problem Definition

This project focuses on exploring new possibilties on string matching algorithm. With currently known algorithms, some of them make use of probability of characters in the text in order to speed up the matching. These algorithms tend to match the character that has the lowest probability first in order to get a mismatch as fast as possible and move on to the next location. For us, we try to leverage both probability and the amount of possible shifting to decide the order of comparison. We want to see if we can achieve faster speed.

## Basic Usage

Clone this repository with

`git clone https://gitlab.com/cs216-projects/string-matching.git`

Go into the project directory and compile with

`make fast`

These will use `-O3` optimization flag during the compilation. We need to use this flag because it can minimize the optimization issue between our own codes and the codes in STL library which should be written pretty optimally. Without this flag, i.e. simply running `make`, we have spotted that STL library have an edge in multiple situations.

After compilation is done, run the executable at

`./build/StringBench.app`

The program supports three different kinds of data. One can set those by using flag `-[acd]` where flag a represents alphabetic data in the language of English, flag c represents the data collected from a corpus of literature, and flag d represents the DNA sequence data. Also, one needs to specify `-p patternLength -t textLength` which corresponding represents the length of the pattern and the text in order to run.

One notice is that, we also make use of the search algorithm from boost library. One needs to have it installed before run the makefile.

Mac user can install it with `brew install boost`

Linux user can install it with `sudo apt-get install libboost-all-dev`

## Selection of Algorithms

We choose and implement a number of algorithms and use those as comparison with the one we are exploring. 
Here is a list of algorithms we use as comparison:
- STL
- Boost
- KMP
- Z
- Horspool
- Probability Horspool
- Sunday Quick Search
- Sunday Optimal Mismatch
- Sunday Maximal Shift

The first four will be used as baseline while the rest will be the main competitors of our algorithm since our algorithm is a modification of those.

## Implementation of Algorithms

- Attempt One (ProbHorspoolTry)

    We modify the Probability Horspool algorithm so that we can leverage both the probability of character from the text and the expected shift distance if a mismatch happens. 
    
    The expected shift is calculated by multiplying the probability of this character with the distance between the current position and the next occurance of this character. By summing up all possible characters, we can get the expected shift of the current position if there is a mismatch. We multiply the expected shift with the probability of a mistch to get the final weight. Then, we sort the pattern according to the weight allowing the position with the highest weight to be compared first. 

    The rest will be exactly the same as normal Horspool algorithm where we jump accordingly to the table we precalculate when a mismatch happens. 

- Attempt Two (SundayTry)
    
    We modify the Sunday Algorithm this time to see if there is a difference. Specically, we modify the Sunday Optimal Mismatch Algorithm. According to the paper, this algorithm also uses the probability of characters in the text as heuristic to find mismatch, but besides the normal shift table calculated in Horspool, it additionally calculates the amount of shift it can make when there is a mismatch based on the already matched character. Thus, it uses the maximum of the two table as the shift value for a mismatch. We apply similar things to modify this algorithm but this time, we try to relax the condition a little bit. Instead of calculating expected shift, we just use index of the position as an approximation of shift. We choose to do it because from the observation of first attempt, it seems that there is no too much of a difference between using a more precise shift amount. Moreover, for a more precise shift amount, it is required to do more computation which adds more time to preprocessing. 
     
## Data Generation

As stated before, we used three sources of data:

- alphabetic frequencies for the English language
- corpus of literature from the Gutenberg project
- sequence of DNA

The data that is generated to be used for each algorithm is explained below:

- alphabetic frequencies for the English language

The sequence is generated using the length of the text as specified when running the program. The frequencies for each character are taken from the internet from Wikipedia. There are only 26 characters which are letters and no uppercase characters, numbers, or punctuations.

- corpus of literature from the Gutenberg project

The sequence is generated using the length of the text as specified when running the program and a randomly generated subsequence of the corpus. The subsequence is taken by randomly generating a position from within the corpus of literature. The corpus contains many books as given by the Gutenberg project. There are 256 characters in the corpus, all represented in ascii.

- sequence of DNA

The sequence is generated using the length of the text as specified when running the program and a randomly generate subsequence of the DNA sequence. The subsequence is taken by randomly generating a position from within the DNA string. There are 4 characters, 'A', 'B', 'C', 'D', in the DNA sequence.

After the text is generated, the pattern to be found in the text, the pattern is generated as a subsequence of this text, with its position being randomly generated. The text for each iteration of the algorithm is different, but if one wishes to text the same pattern and text for many iterations they may use the  `-s` seed flag and set the seed value.

## Benchmark Design

We utilize inheritance of C++ so that all algorithms will be evaluated with the same interface. We meassure the following three things. First, we measure the number of the comparisons made on charaters during the string matching. This is done by invoking our own comparison functions so that we can increment the counter during the comparison. However, we cannot do it for STL and Boost library since their functions are a black box. Second, we keep track of the preprocess time of each algorithm. This is done by using high resolution timer from C++ around the preprocess function of each algorithm. Lastly, we measure the matching time whose configuration is similar to the above step. 

For each execution of the benchmark application, we run the test for a thousand times with the input pattern length and text length. In each iteration, the text is randomly generated and the pattern is a substring randomly picked from the text. We will try to find every possible position of the pattern in the text. We take the average of comparisons, preprocessing time and matching time, and output them when the program ends. 

## Testing Results

We have written shell scripts to run experiments and do analysis. One can go into analyze folder the run the following scripts.

`./experiment.sh [pattern|text] StartValue EndValue StepSize [DNA|Corpus|Alpha]`

The first argument is the parameter one wants to test. One can change either pattern length or text length with the other one fixed. The following three are the start, end and the step size for the testing parameter. The last is the data set used in this testing.

After running the above script, one can run the following to get the analysis graphs.

`./analyze.sh [pattern|text] StartValue EndValue StepSize [DNA|Corpus|Alpha] [total|match|comp]`

The first five parameters are the same with the last to specify the result field we are looking at. Total stands for the total runing time, and match stands for the matching only, and comp stands for the number of comparisons. Here are some of the results we have found.

![DNA Total](https://gitlab.com/cs216-projects/string-matching/uploads/2ec81c8555bc9c5241b7e98faaad1631/PLen_DNA_Total.png "DNA Total")
![DNA Matching](https://gitlab.com/cs216-projects/string-matching/uploads/ffdd9df0aab12d3d80bf016fea094495/PLen_DNA_Match.png "DNA Matching")

The above two graphs show the total time and matching time of pattern length ranging from 10 to 200 with 10 as step size in DNA data. 

For total time, we can clearly see that both ProbHorspoolTry and SundayTry outperform the baseline algorithm like Z, STL, KMP and Boost. For ProbHorspoolTry, it outperforms the Probability Horspool Algorithm which means that taking shift length in cosideration does better than use pure probabilty. However, it is interesting that it is even worse than the normal Horspool algorithm which does not use any heuristic at all. We can see from the Matching time graph that the ProbHorspoolTry algorithm only outperforms the normal horspool by a tiny bit if the pattern length is greater than 50. However, the gain is too little to recover the lost in the preprocessing time. 

On the other hand, the Sunday OM and MS, together with our SundayTry outperform all the other algorithms. However, it is not clear that our algorithm can consistently outperforms the OM and MS for both total time and matching time. But it seems that our algorithm has an edge in total time when the pattern length is greater than 160, but the matching time does not show the same.

![Corpus Total](https://gitlab.com/cs216-projects/string-matching/uploads/cd8244d6566f2e2c0d823b830112fe40/PLen_Corpus_Total.png "Corpus Total")
![Corpus Match](https://gitlab.com/cs216-projects/string-matching/uploads/318c18fb5f74c6f06fd9ddfd5447cfac/PLen_Corpus_Match.png "Corpus Match")

When using Courpus data, we get pretty different results even though both of them still perform better than the baseline. First, ProbHorspoolTry does not get any better during the matching phase than Probability Horspool. However, since it needs more time to do preprocessing, we can see that it is slower than Probability Horspool in the total time. Second, SundayTry still do not have a consistent edge against Sunday OM and MS. They are pretty close all the time. One thing that is interesting is that the algorithm that do not use probability heuristics perform better in the total time. This is due to the fact that they are just a tiny bit behind during the matching phase. However, since they both do not require too much preprocessing, both Horspool and Sunday QS outperforms the rest consistently when the pattern length is greater than 80. 

![Alpha Total](https://gitlab.com/cs216-projects/string-matching/uploads/c7a4f89cea0a23892da32cfc143c62e9/PLen_Alpha_Total.png "Alpha Total")
![Alpha Match](https://gitlab.com/cs216-projects/string-matching/uploads/eeb97fc9c8e198e119fb3bb94839d8ec/PLen_Alpha_Match.png "Alpha Match")

The results for Alphabet data is similar with Corpus since both of them comes from statistics from literature or natural language. The algorithms without heuristics, i.e. Horspool and Sunday QS, outperform the rest of algorithms since they only lose a tiny bit in mathcing time, and the preprocessing time is low much lower than those that need to sort with probabilities. 

## References

1. [Fast string matching by using probabilities: On an optimal mismatch variant of Horspool's algorithm](https://www.sciencedirect.com/science/article/pii/S0304397506003124)
2. [A Very Fast Substring Search Algorithm](http://www.quretec.com/u/vilo/edu/2002-03/Tekstialgoritmid_I/Articles/Exact/Sunday_p132-sunday.pdf)

## Authors (In Alphabetical Order)

- Alan Tang ()
- Howard Huang (704429216)
- Jayanth (405026111)
- Shuang Zhang (804296230)
- Yanbiao Li