BUILD_DIR ?= build
SRC_DIRS := src

MAKE_BUILD := $(shell mkdir -p $(BUILD_DIR))
SRCS := $(shell find $(SRC_DIRS) -name "*.cpp")
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

INC_DIRS  := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CPPFLAGS ?= $(INC_FLAGS) -MMD -MP -std=c++11
CC       := g++

all: bench

test:
	@echo $(OBJS)

fast : CC = g++ -O3
fast : bench

bench: $(BUILD_DIR)/StringBench.app

$(BUILD_DIR)/StringBench.app: $(OBJS)
	$(CC) $(OBJS) -o $@

# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CC) $(CPPFLAGS) -c $< -o $@


.PHONY: clean
clean:
	$(RM) -r $(BUILD_DIR)

-include $(DEPS)

MKDIR_P ?= mkdir -p
