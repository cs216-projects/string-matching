
#include <kmp.hpp>
#include <string>
#include <vector>

/**
* More information here : https://www.geeksforgeeks.org/searching-for-patterns-set-2-kmp-algorithm/
*/
namespace smb{
    void kmp::preProcessing(){
        int pattern_length = m_pattern.length();
        prefixArray.resize(pattern_length);
        int index =0;
        for(int i=1; i < pattern_length;){
            if(cmpChars(m_pattern[i], m_pattern[index])){ //pattern match, increase i and index storing index+1
                prefixArray[i] = index + 1;
                index++;
                i++;
            }else{ //pattern din't match, go to prefix match position of 1 index back and recompare in next iteration
                if(index != 0){
                    index = prefixArray[index-1];
                }else{ //base case during start
                    prefixArray[i] =0;
                    i++;
                }
            }
        }
    }

    void kmp::findOne(int& firstPosition){
        int pattern_length = m_pattern.length();
        int text_length = m_text.length();

        int i=0; //iterator over text
        int j=0; //iterator over pattern
        while(i < text_length){
            if(j==pattern_length){ //the pattern completely matched
                  firstPosition = i-j;
                  return;
            }else if(cmp(j,i)){
                  i++;
                  j++;
            }else{
                if(j!=0){ //the pattern did not match so jump to prefix array position of last character and recompare in next iteration
                    j = prefixArray[j-1];
                }else{ //the pattern did not match but in the start of pattern so move to next character for comparision in text
                    i++;
                }
            }
        }
        if(j==pattern_length){ //the pattern completely matched
            firstPosition = i-j;
            return;
        }
    }

    void kmp::findAll(std::vector<int>& allPositions){
        int pattern_length = m_pattern.length();
        int text_length = m_text.length();

        int i=0; //iterator over text
        int j=0; //iterator over pattern
        while(i < text_length){
            if(j==pattern_length){ //the pattern completely matched
                allPositions.push_back(i-j);
                j = prefixArray[j-1];
            }else if(cmp(j,i)){
                i++;
                j++;
            }else{
                if(j!=0){ //the pattern did not match so jump to prefix array position of last character and recompare in next iteration
                    j = prefixArray[j-1];
                }else{ //the pattern did not match but in the start of pattern so move to next character for comparision in text
                    i++;
                }
            }
        }
        if(j==pattern_length){ //the pattern completely matched
            allPositions.push_back(i-j);
            j = prefixArray[j-1];
        }
    }
}
