
#include <z.hpp>
#include <string>
#include <vector>

/**
* Demo here : http://www.utdallas.edu/~besp/demo/John2010/z-match.htm
* More information here : https://www.geeksforgeeks.org/z-algorithm-linear-time-pattern-searching-algorithm/
*/
namespace smb{

    void z::preProcessing(){
        // Create concatenated string "P$T"
        m_concat = m_pattern + "$" + m_text;
    }

    void z::createZarray(std::vector<int>& Z){
        int concat_length = m_concat.length();
        int left = 0;
        int right = 0;
        for(int k = 1; k < concat_length; k++) {
            if(k > right) {
                left = right = k;
                while(right < concat_length && cmpChars(m_concat[right], m_concat[right - left])) {
                    right++;
                }
                Z[k] = right - left;
                right--;
            } else {
                //we are operating inside box
                int k1 = k - left;
                //if value does not stretches till right bound then just copy it.
                if(Z[k1] < right - k + 1) {
                    Z[k] = Z[k1];
                } else { //otherwise try to see if there are more matches.
                    left = k;
                    while(right < concat_length && cmpChars(m_concat[right], m_concat[right - left])) {
                        right++;
                    }
                    Z[k] = right - left;
                    right--;
                }
            }
        }
    }

    void z::findOne(int& firstPosition){
        int concat_length = m_concat.length();
        int pattern_length = m_pattern.length();

        std::vector<int> Z(concat_length);
        createZarray(Z);

        for(int i = 0; i < concat_length ; i++) {
            if(Z[i] == pattern_length) {
                firstPosition = i - pattern_length - 1;
                return;
            }
        }
    }

    void z::findAll(std::vector<int>& allPositions){
        int concat_length = m_concat.length();
        int pattern_length = m_pattern.length();

        std::vector<int> Z(concat_length);
        createZarray(Z);

        for(int i = 0; i < concat_length ; i++) {
            if(Z[i] == pattern_length) {
                allPositions.push_back(i - pattern_length - 1);
            }
        }
    }

}
