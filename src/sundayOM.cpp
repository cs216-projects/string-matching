#include <sundayOM.hpp>
#include <numeric>
#include <algorithm>

namespace smb {
// namespace for string-match benchmark

void SundayOM::preProcessing()
{
  int patternLength = m_pattern.size();
  // Build TD1
  for(int ch = 0; ch < 256; ch++)
  {
    TD1[ch] = patternLength + 1;
  }
  for(int pos = 0; pos < patternLength; pos++)
  {
    TD1[m_pattern[pos]] = patternLength - pos;
  }

  // Create ordering based on frequency
  patternOrder.resize(patternLength);
  std::iota (patternOrder.begin(), patternOrder.end(), 0);

  struct Comparer{
    double *ratios;

    Comparer(double *r) { ratios = r; }
    bool operator() (int a, int b) {
        return ratios[a] < ratios[b];
    }
  } comparer(SundayOM::CharacterRatio);

  std::sort(patternOrder.begin(), patternOrder.end(), comparer);

  // Initialize TD2
  TD2.resize(patternLength);
  int lshift = 1;
  for (int j = 0; j < patternLength; j++)
  {
    // Find smallest possible shift that could match
    for (int newShift = lshift; newShift <= patternLength; newShift++)
    {
      bool canMatch = true;
      for (int i = 0; canMatch && i < j; i++)
      {
        int pos = patternOrder[i];
        canMatch = canMatch && (pos < newShift || m_pattern[pos] == m_pattern[pos - newShift]);
      }
      int pos = patternOrder[j];
      canMatch = canMatch && (pos < newShift || m_pattern[pos] != m_pattern[pos - newShift]);

      if(canMatch)
      {
        lshift = newShift;
        break;
      }
    }

    TD2[j] = lshift;
  }

}

void SundayOM::findOne(int& firstPosition)
{
  int m = m_pattern.size();
  int n = m_text.size();
  int k = 0;

  while (k + m < n)
  {
    int j = 0;
    while (j < m && cmp(patternOrder[j], k + patternOrder[j])) 
    {
      j++;
    }
    if (j == m)
    {
      firstPosition = k;
      return;
    }
    int delta1 = TD1[ m_text[k + m] ];
    int delta2 = TD2[j];
    k += delta1 > delta2 ? delta1 : delta2;
  }
}

void SundayOM::findAll(std::vector<int>& allPositions)
{
  int m = m_pattern.size();
  int n = m_text.size();
  int k = 0;

  while (k + m < n)
  {
    int j = 0;
    while (j < m && cmp(patternOrder[j], k + patternOrder[j])) 
    {
      j++;
    }
    if (j == m)
    {
      allPositions.push_back(k);
      k++;
    }
    else
    {
      int delta1 = TD1[ m_text[k + m] ];
      int delta2 = TD2[j];
      k += delta1 > delta2 ? delta1 : delta2;
    }
  }
}

}// namespace smb
