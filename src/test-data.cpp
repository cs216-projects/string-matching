#include <test-data.hpp>
#include <iostream>
#include <fstream>
#include <stdlib.h> /* srand, rand */
#include <time.h> /* time */
#include <random>
#include <map>
#include <cstdlib> /* system */

namespace smb {

std::vector<char>letters{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
std::vector<double>letterFrequencies{.08167,  .01492,  .02782, .04253, .12702, .02228, .02015, .06094, .06966, .00153, .00772, .04025, .02406, .06749, .07507, .01929, .00095, .05987, .06327, .09056, .02758, .00978, .02360, .00150, .01974, .00074};
std::piecewise_constant_distribution<> letterDistribution;

std::string dnaString;
std::string corpusString;

DataType currentDataType;

std::string loadCharacterRatiosHelper(std::string fileName) {
  std::string fileString("");
  std::map<char, int> characterCounts;
  std::ifstream datafile(fileName);

  std::string line;
  while (std::getline(datafile, line))
  {
    // for character frequencies
    if(line.length()==0)continue;
    for (int i = 0; i < line.length() - 1; i++) {
      characterCounts[line[i]] += 1;
    }

    // save the file as a string, removes the new line character
    if (!line.empty()) {
      line.pop_back();
      fileString += line;
    }
  }
  int count = 0;
  for(auto elem : characterCounts)
  {
    count += elem.second;
  }
  for(auto elem : characterCounts)
  {
    StringMatchingAlgorithm::CharacterRatio[int(elem.first)] = double(elem.second)/double(count);
  }
  return fileString;
}

void loadCharacterRatios(DataType type) {
  currentDataType = type;
  if (type == Alphabet) {
    // load character ratios for the alphabet
    for (int i = 0; i < 26; i++) {
      StringMatchingAlgorithm::CharacterRatio[int(letters[i])] = letterFrequencies[i];
    }
  } else if (type == DNA) {
    // load character ratios for DNA
    dnaString = loadCharacterRatiosHelper("data/dna_2.txt");
  } else if (type == Corpus) {
    // load character ratios for corpus
    corpusString = loadCharacterRatiosHelper("data/corpus.txt");
  }
}

void
initializeTestInstance()
{
  srand (time(NULL));

  // generate 1 unit intervals from 0 to 27, each representing a character, e.g. A = 0-1, B= 1-2, ...
  std::vector<double> interval;
  for (double i = 0; i <= 27; i++) {
      interval.push_back(i);
  }
  letterDistribution = std::piecewise_constant_distribution<>(interval.begin(), interval.end(), letterFrequencies.begin());
}

std::tuple<std::string, std::string>
getNextTestCase(int patternLength, int textLength, int seed)
{
  if (patternLength == 0) {
    patternLength = 4 + rand() % 17;
  }
  if (textLength == 0) {
    textLength = 100 + rand() % 1000001;
  }

  std::string pattern("");
  std::string text("");

  std::mt19937 rng;
  if (seed == 0) {
    // randomly seeded mersenne twister engine
    rng = std::mt19937(std::random_device{}());
  } else {
    // use provided seeded value
    rng = std::mt19937(seed);
  }

  if (currentDataType == Alphabet) {
    // alphabet
    // self generate text
    for (int i = 0; i < textLength; i++) {
        text += letters[(int)letterDistribution(rng)];
    }
    // collect pattern from text
    std::uniform_int_distribution<int> uniformDistribution(0, textLength - patternLength);
    auto randomInt = uniformDistribution(rng);
    pattern = text.substr(randomInt, patternLength);
  } else if (currentDataType == DNA) {
    // DNA
    // collect text from data
    std::uniform_int_distribution<int> uniformDistributionText(0, dnaString.length() - textLength);
    auto randomIntText = uniformDistributionText(rng);
    text = dnaString.substr(randomIntText, textLength);
    // collect pattern from data
    std::uniform_int_distribution<int> uniformDistributionPattern(0, text.length() - patternLength);
    auto randomIntPattern = uniformDistributionPattern(rng);
    pattern = text.substr(randomIntPattern, patternLength);
  } else if (currentDataType == Corpus) {
    // Corpus
    // collect text from data
    std::uniform_int_distribution<int> uniformDistributionText(0, corpusString.length() - textLength);
    auto randomIntText = uniformDistributionText(rng);
    text = corpusString.substr(randomIntText, textLength);
    // collect pattern from data
    std::uniform_int_distribution<int> uniformDistributionPattern(0, text.length() - patternLength);
    auto randomIntPattern = uniformDistributionPattern(rng);
    pattern = text.substr(randomIntPattern, patternLength);
  }

  return std::make_tuple(pattern, text);
}

void
displayBenchmarkResult(const std::unordered_map<std::string, StringMatchingMetrics>& result, std::string fileName)
{
  // pass results as argument to python script
  std::string commandLineArgument("Algorithm Name, Number of Character Comparisons, Number of Character Comparisons Matched, Number of Character Comparisons Mismatched, Preprocessing Time, String Matching Time\n");
  for (auto& elem : result) {
    commandLineArgument += elem.first + "," + std::to_string(elem.second.nCharacterComparisonMatched + elem.second.nCharacterComparisonMismatched) + ","
    + std::to_string(elem.second.nCharacterComparisonMatched) + "," + std::to_string(elem.second.nCharacterComparisonMismatched) + ","
      + std::to_string(elem.second.dPreProcessing) + "," + std::to_string(elem.second.dMatching) + "\n";
  }

  // remove last "|"
  if (!commandLineArgument.empty())
    commandLineArgument.pop_back();

  commandLineArgument = "\"" + commandLineArgument + "\"  \"" + fileName + "\"";

  // run python script
  std::system(("python ./benchmark-results/script.py " +  commandLineArgument).c_str());
}

} // namespace smb
