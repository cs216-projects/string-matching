/**
 * @file   algo-base.cpp
 * @author Yanbiao Li <lybmath@cs.ucla.edu>
 * @date   Sun May 20 21:37:49 2018
 *
 * @brief  the base class implementation of string matching algorithms
 *
 */
#include <algo-base.hpp>
#include <chrono>
#include <ratio>

namespace smb {

double StringMatchingAlgorithm::CharacterRatio[256];

StringMatchingMetrics
StringMatchingAlgorithm::run(const std::string& pattern,
			     const std::string& text,
			     bool needFindAll)
{
  std::chrono::high_resolution_clock::time_point start;
  std::chrono::duration<double, std::micro> processTime, matchTime;
  std::vector<int> allPositions;
  int firstPosition = -1;

  // assign strings
  m_pattern = pattern;
  m_text = text;

  // pre processing
  start = std::chrono::high_resolution_clock::now();
  preProcessing();
  processTime = std::chrono::high_resolution_clock::now() - start;

  // string matching
  m_nCharacterComparisonMatched = 0;
  m_nCharacterComparisonMismatched = 0;
  start = std::chrono::high_resolution_clock::now();
  if (needFindAll) {
    findAll(allPositions);
  }
  else {
    findOne(firstPosition);
  }
  matchTime = std::chrono::high_resolution_clock::now() - start;

  // return result
  return StringMatchingMetrics(needFindAll ? check(allPositions) : check(firstPosition),
			       m_nCharacterComparisonMatched,
             m_nCharacterComparisonMismatched,
			       processTime.count(),
			       matchTime.count());
}

bool
StringMatchingAlgorithm::check(int position)
{
  // start from 0
  if (position < 0 || position + m_pattern.size() > m_text.size()) {
    fprintf(stderr, "ERROR: the position %d is invalid\n", position);
    return false;
  }

  for (int offset = m_pattern.size() - 1; offset >= 0; offset --) {
    if (m_pattern[offset] != m_text[position + offset]) {
      fprintf(stderr, "ERROR: at position %d: pattern[%d] != text[%d + %d]\n",
	      position, offset, offset, position);
      return false;
    }
  }

  return true;
}

bool
StringMatchingAlgorithm::check(const std::vector<int>& allPositions)
{
  if (allPositions.empty()) {
    return false;
  }

  for (const auto& pos : allPositions) {
    if (!check(pos)) {
      return false;
    }
  }

  return true;
}

}// namespace smb
