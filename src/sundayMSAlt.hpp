#include <algo-base.hpp>
#include <utility>

namespace smb {

class SundayMSAlt : public StringMatchingAlgorithm
{
private:
  int TD1[256];
  std::vector<int> TD2;
  std::vector<std::pair<int,char>> patternOrder;
  std::vector<int> minShift;

protected:
  /**
   * pre-processing before start string matching
   *
   */
  void preProcessing();
  int matchShift(int ploc, int lshift);

  /**
   * find the first appearance of the pattern in the text
   *
   * @param firstPosition capture out the first appearing position (start from 0)
   */
  void findOne(int& firstPosition);

  /**
   * find all appearances of the pattern in the text
   *
   * @param allPositions capture out all appearing positions
   */
  void findAll(std::vector<int>& allPositions);
};

}// namespace smb
