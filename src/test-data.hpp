#include <tuple>
#include <string>
#include <unordered_map>
#include <algo-base.hpp>

namespace smb {

/** 
 * types of data supported
 * 
 */
enum DataType {
  Alphabet = 0,
  DNA,
  Corpus,
};

/** 
 * load character ratios based on the type of data you are testing
 * 
 */
void
loadCharacterRatios(DataType type);

/** 
 * perform anything here to initialize one instance for a couple of test cases  
 * 
 */
void
initializeTestInstance();

/** 
 * generate or fetch the next test case
 * 
 * @param patterLength if it's 0, use a randomized length, in the range [4, 20] 
 * @param textLength if it's 0, use a randomized length, in the range [100, 1000100]
 * 
 * @return 
 */
std::tuple<std::string, std::string>
getNextTestCase(int patternLength = 0, int textLength = 0, int seed = 0);

/** 
 * display the benchmark results
 * 
 * @param result the benchmark result
 */
void
displayBenchmarkResult(const std::unordered_map<std::string, StringMatchingMetrics>& result, std::string fileName = "sample.csv");

} // namespace smb
