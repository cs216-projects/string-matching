
#include <horspool.hpp>
#include <string>
#include <vector>

namespace smb{
    void horspool::preProcessing(){
        int patternLength = m_pattern.size();
        for(int i = 0; i < 256; ++i){
            preprocessTable[i] = patternLength;
        }
        for(int i = 0; i < patternLength - 1; ++i){
            preprocessTable[m_pattern[i]] = patternLength - 1 - i;
        }
    }

    void horspool::findOne(int& firstPosition){
        int skip = 0;
        int textLength = m_text.size();
        int patternLength = m_pattern.size();
        while(textLength - skip >= patternLength){
            int i = patternLength - 1;
            int j = skip + i;
            while(cmp(i,j)){
                if(i == 0){
                    firstPosition = skip;
                    return;
                }
                --i;
                --j;
            }
            skip = skip + preprocessTable[m_text[skip + patternLength - 1]];
        }
    }

    void horspool::findAll(std::vector<int>& allPositions){
        int skip = 0;
        int textLength = m_text.size();
        int patternLength = m_pattern.size();
        while(textLength - skip >= patternLength){
            int i = patternLength - 1;
            int j = skip + i;
            while(cmp(i,j)){
                if(i == 0){
                    allPositions.push_back(skip);
                    break;
                }
                --i;
                --j;
            }
            skip = skip + preprocessTable[m_text[skip + patternLength - 1]];
        }
    }
}
