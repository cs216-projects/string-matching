/**
 * @brief  a wapper of Boost string matching algorithm
 */
#include <boostalgo.hpp>
#include <string>
#include <boost/algorithm/string.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <boost/range/iterator_range_core.hpp>

namespace smb {
// namespace for string-match benchmark

void
BoostAlgo::preProcessing()
{
}

void
BoostAlgo::findOne(int& firstPosition)
{
  auto result=boost::find_first(m_text,m_pattern);
  firstPosition = result.begin() - m_text.begin(); //if slow, will optimize with index adaptor
}

void
BoostAlgo::findAll(std::vector<int>& allPositions)
{
  typedef std::vector<boost::iterator_range<std::string::iterator>> string_range_vector;

  string_range_vector result;

  boost::find_all(result, m_text,m_pattern); //if slow, will optimize with index adaptor

  for (auto match: result){
    allPositions.push_back(match.begin() - m_text.begin()); //can also use range difference
  }
}

}// namespace smb
