/**
 * @file   stl.cpp
 * @author Yanbiao Li <lybmath@cs.ucla.edu>
 * @date   Sun May 20 21:59:17 2018
 *
 * @brief  a wapper of STL string matching algorithm
 *
 */
#include <stl.hpp>
#include <string>
#include <iostream>

namespace smb {
// namespace for string-match benchmark

void
STL::preProcessing()
{
}

void
STL::findOne(int& firstPosition)
{
  firstPosition = m_text.find(m_pattern);
}

void
STL::findAll(std::vector<int>& allPositions)
{
  int pos = m_text.find(m_pattern);
  while (pos != std::string::npos) {
    allPositions.push_back(pos);
    pos = m_text.find(m_pattern, pos + m_pattern.size());
  }
}

}// namespace smb
