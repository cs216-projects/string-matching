#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <unordered_map>
#include <algorithm> // equal
#include <iostream>
#include <cstdlib> // for atoi

#include <test-data.hpp>
#include <stl.hpp> // include stl string matching algorithm
#include <horspool.hpp>
#include <probHorspool.hpp>
#include <sundayQS.hpp>
#include <sundayMS.hpp>
#include <sundayOM.hpp>
#include <z.hpp>
#include <kmp.hpp>
#include <boostalgo.hpp>
#include <probHorspoolP.hpp>
#include <probHorspoolTry.hpp>
#include <sundayOMAlt.hpp>
#include <sundayMSAlt.hpp>
#include <sundayTry.hpp>

namespace smb {
// namespace for string-match benchmark

class StringMatchingBenchmark
{
public:
  ~StringMatchingBenchmark()
  {
    for (auto& entry : algoList) {
      if (entry.second) {
	delete entry.second;
	entry.second = NULL;
      }
    }
  }

public:
  void
  addAlgorithm(StringMatchingAlgorithm* algorithm, const std::string& name)
  {
    if (algoList.find(name) != algoList.end()) {
      fprintf(stderr, "algorithm %s has already been added\n", name.c_str());
      return;
    }

    algoList[name] = algorithm;
  }

  std::unordered_map<std::string, StringMatchingMetrics>
  runBenchmark(const std::string& pattern, const std::string& text, bool needFindAll = false)
  {
    std::unordered_map<std::string, StringMatchingMetrics> results;
    for (auto& entry : algoList) {
      results[entry.first] = entry.second->run(pattern, text, needFindAll);
    }
    return results;
  }

private:
  std::unordered_map<std::string, StringMatchingAlgorithm*> algoList;
};

}// namespace smb

int usage()
{
  fprintf(stderr, "Usage: ./build/StringBench.app [options]\n"
	  "-h\t print this usage\n"
	  "-a\t set DataType to Alphabet\n"
	  "-d\t set DataType to DNA\n"
	  "-c\t set DataType to Corpus\n"
	  "-p [pl]\t set pattern length to pl\n"
	  "-t [tl]\t set text length to tl\n"
    "-s [sv]\t set seed value to sv\n");
  return 0;
}

int main(int argc, char** argv)
{
  int argPatternLength = 20;
  int argTextLength = 500;
  int argSeedValue = 0;
  smb::DataType argDataType = smb::Alphabet;

  int c;
  while ((c = getopt (argc, argv, "adcp:t:s:h")) != -1) {
    switch (c) {
    case 'a': argDataType = smb::Alphabet; break;
    case 'd': argDataType = smb::DNA; break;
    case 'c': argDataType = smb::Corpus; break;
    case 'p': argPatternLength = std::atoi(optarg); break;
    case 't': argTextLength = std::atoi(optarg); break;
    case 's': argSeedValue = std::atoi(optarg); break;
    case 'h':
    default: return usage();
    }
  }

  // load the character ratio
  smb::loadCharacterRatios(argDataType);
  // for (int i = 0; i < sizeof(smb::StringMatchingAlgorithm::CharacterRatio) / sizeof(double); i++) {
  //   double freq = smb::StringMatchingAlgorithm::CharacterRatio[i];
  //   if (freq != 0) std::cout << char(i) << " " << freq << "\n";
  // }

  // add algorithm to benchmark
  smb::StringMatchingBenchmark stringBench;
  stringBench.addAlgorithm(new smb::STL, "STL");
  stringBench.addAlgorithm(new smb::horspool, "horspool");
  stringBench.addAlgorithm(new smb::probHorspool, "probHorspool");
  stringBench.addAlgorithm(new smb::sundayQS, "sundayQS");
  //stringBench.addAlgorithm(new smb::SundayMS, "SundayMS");
  //stringBench.addAlgorithm(new smb::SundayOM, "SundayOM");
  stringBench.addAlgorithm(new smb::z, "Z");
  stringBench.addAlgorithm(new smb::kmp, "KMP");
  stringBench.addAlgorithm(new smb::BoostAlgo, "BoostAlgo");
  stringBench.addAlgorithm(new smb::probHorspoolP, "probHorspoolP");
  stringBench.addAlgorithm(new smb::probHorspoolTry, "probHorspoolTry");
  stringBench.addAlgorithm(new smb::SundayOMAlt, "SundayOMAlt");
  stringBench.addAlgorithm(new smb::SundayMSAlt, "SundayMSAlt");
  stringBench.addAlgorithm(new smb::SundayTry, "SundayTry");

  // example for running just one test
  smb::initializeTestInstance();
  std::string pattern, text;
  const int TEST_ITERATIONS = 1000;
  const int PATTERN_LENGTH = argPatternLength;
  const int TEXT_LENGTH = argTextLength;
  std::unordered_map<std::string, smb::StringMatchingMetrics> results;
  for(int i = 0; i < TEST_ITERATIONS; ++i){
      if (argSeedValue == 0) {
        // if seed value is 0, then have the patterns and texts be generated randomly for each iteration
        std::tie(pattern, text) = smb::getNextTestCase(PATTERN_LENGTH, TEXT_LENGTH);
      } else {
        // otherwise, use the given seeded value
        std::tie(pattern, text) = smb::getNextTestCase(PATTERN_LENGTH, TEXT_LENGTH, argSeedValue);
      }
      auto res = stringBench.runBenchmark(pattern, text, true);
      for(auto iter = res.begin(), ater = res.end(); iter!=ater; ++iter){
          if(results.count(iter->first)){
              if(!iter->second.isCorrect){
                  results[iter->first].isCorrect = false;
              }
              results[iter->first].nCharacterComparisonMatched += iter->second.nCharacterComparisonMatched;
              results[iter->first].nCharacterComparisonMismatched += iter->second.nCharacterComparisonMismatched;
              results[iter->first].dPreProcessing += iter->second.dPreProcessing;
              results[iter->first].dMatching += iter->second.dMatching;
          }
          else{
              results[iter->first] = iter->second;
          }
      }
  }
  for(auto iter=results.begin(), ater=results.end(); iter!=ater; ++iter){
      iter->second.nCharacterComparisonMatched /= TEST_ITERATIONS;
      iter->second.nCharacterComparisonMismatched /= TEST_ITERATIONS;
      iter->second.dPreProcessing /= TEST_ITERATIONS;
      iter->second.dMatching /= TEST_ITERATIONS;
  }

  // sort the results
  typedef std::vector<std::pair<std::string, smb::StringMatchingMetrics> > SortableResults;
  SortableResults sortedResults(results.begin(), results.end());
  std::sort(sortedResults.begin(), sortedResults.end(),
       [] (const SortableResults::value_type& x, const SortableResults::value_type& y) {
	      return x.second < y.second;
       });

  for (auto& entry : sortedResults) {
    fprintf(stderr, "%20s: ", entry.first.c_str());
    entry.second.display();
  }

  // display the results
  displayBenchmarkResult(results);

  return 0;
}
