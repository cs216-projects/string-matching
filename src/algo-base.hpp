/**
 * @file   algo-base.hpp
 * @author Yanbiao Li <lybmath@cs.ucla.edu>
 * @date   Sun May 20 21:35:42 2018
 *
 * @brief  the base class definition of string matching algorithms
 *
 */

#ifndef ALGOBASE_HPP
#define ALGOBASE_HPP

#include <string>
#include <vector>
#include <cmath> // for isless, abs

namespace smb {
// namespace for string-match benchmark

struct StringMatchingMetrics
{
  bool   isCorrect;            // whether a matching can be found
  //double nCharacterComparison; // the number of comparisons
  double nCharacterComparisonMatched; // the number of comparisons which matched
  double nCharacterComparisonMismatched; // the number of comparisons which mismatched
  double dPreProcessing;       // the time spent on pre-processing
  double dMatching;            // the time spent on string matching

  StringMatchingMetrics(bool b = false, int nm1 = 0, int nm2 = 0, double d1 = 0, double d2 = 0)
    : isCorrect(b), nCharacterComparisonMatched(nm1), nCharacterComparisonMismatched(nm2), dPreProcessing(d1), dMatching(d2) {};

  void display()
  {
    if (!isCorrect) {
      fprintf(stderr, "CAN NOT FIND ANY MATCHING\n");
      return;
    }

    fprintf(stderr,
	    "# of comparisons: %8.0f, # of comparisons matched: %8.0f, # of comparisons mismatched: %8.0f, " 
      "processing time: %8.2f us, matching time: %8.2f us, total time: %8.2f\n", 
      nCharacterComparisonMatched + nCharacterComparisonMismatched, nCharacterComparisonMatched, nCharacterComparisonMismatched, 
      dPreProcessing, dMatching, dPreProcessing + dMatching);
  }

  friend bool operator < (const StringMatchingMetrics&x, const StringMatchingMetrics&y)
  {
    auto x_tot = x.dPreProcessing + x.dMatching;
    auto y_tot = y.dPreProcessing + y.dMatching;
    if (std::isless(std::abs(x_tot - y_tot), 1e-15)) {
      return std::isless(x.dMatching, y.dMatching);
    }
    else {
      return std::isless(x_tot, y_tot);
    }
  }
};

class StringMatchingAlgorithm
{
public:
  static double CharacterRatio[256];
  virtual ~StringMatchingAlgorithm() {};

public: // the interface to be invoked from the benchmark
  /**
   * run a test instance for an algorithm
   *
   * @param pattern the pattern string
   * @param text the text string
   * @param needFindAll whether need to find all appearance of the pattern
   *
   * @return evaluation metrics of running this test instance
   */
  StringMatchingMetrics
  run(const std::string& pattern, const std::string& text, bool needFindAll = false);

protected: // the interfaces that must be implemented in sub classes
  /**
   * pre-processing before start string matching
   *
   */
  virtual void preProcessing() = 0;

  /**
   * find the first appearance of the pattern in the text
   *
   * @param firstPosition capture out the first appearing position (start from 0)
   */
  virtual void findOne(int& firstPosition) = 0;

  /**
   * find all appearances of the pattern in the text
   *
   * @param allPositions capture out all appearing positions
   */
  virtual void findAll(std::vector<int>& allPositions) = 0;

protected: // helper functions that will be used in subclasses
  /**
   * all subclass implementations must invoke this function to compare two characters from
   * the pattern and the text respectively.
   *
   * the number of comparisons is increased by one
   *
   * @param patternIdx the index of the character in the pattern (start from 0)
   * @param textIdx the index of the character in the text (start from 0)
   *
   * @return whether
   */
  bool cmp(const int& patternIdx, const int& textIdx);
  bool cmpChars(const char& patternChar, const char& textChar);

private: // helper functions that are only invoked in this class
  bool check(int position);
  bool check(const std::vector<int>& allPositions);

protected: // members that will be used in subclasses
  std::string m_pattern;
  std::string m_text;

private: // members that will only be used in this class
  //int m_nCharacterComparison;
  int m_nCharacterComparisonMatched;
  int m_nCharacterComparisonMismatched;
};

inline bool
StringMatchingAlgorithm::cmp(const int& patternIdx, const int& textIdx)
{
  bool result = (m_pattern[patternIdx] == m_text[textIdx]);
  //m_nCharacterComparison++;
  if(result)
    m_nCharacterComparisonMatched++;
  else
    m_nCharacterComparisonMismatched++;
  return result;

}

//required for Z algorithm as it compares characters from custom created array from text and pattern
inline bool
StringMatchingAlgorithm::cmpChars(const char& patternChar, const char& textChar)
    {
      bool result = (patternChar == textChar);
      //m_nCharacterComparison++;
      if(result)
        m_nCharacterComparisonMatched++;
      else
        m_nCharacterComparisonMismatched++;
      return result;
    }

}// namespace smb

#endif
