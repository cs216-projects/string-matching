#include <algo-base.hpp>
#include <vector>

namespace smb{
    class probHorspoolP: public StringMatchingAlgorithm{
    private:
        int preprocessTable[256];
        std::vector<int> order;
        double CharacterRatioP[256];
    protected:
      /**
       * pre-processing before start string matching
       *
       */
      void preProcessing();

      /**
       * find the first appearance of the pattern in the text
       *
       * @param firstPosition capture out the first appearing position (start from 0)
       */
      void findOne(int& firstPosition);

      /**
       * find all appearances of the pattern in the text
       *
       * @param allPositions capture out all appearing positions
       */
      void findAll(std::vector<int>& allPositions);
    };

}
