
#include <probHorspoolTry.hpp>
// #include <queue>
// #include <stack>
// #include <deque>
#include <utility>
#include <unordered_map>
#include <algorithm>

namespace smb{
    void probHorspoolTry::preProcessing(){
        // int closeIdx[256];
        // std::vector<std::stack<int>> idxQ(256,std::stack<int>());
        // int patternLength = m_pattern.size();
        // order.clear();
        // order.resize(patternLength);
        // for(int i = 0; i < 256; i++){
        //     preprocessTable[i] = patternLength;
        //     closeIdx[i] = -1;
        // }
        // for(int i = 0; i < patternLength - 1; ++i){
        //     preprocessTable[m_pattern[i]] = patternLength - 1 - i;
        //     idxQ[m_pattern[i]].push(i);
        //     closeIdx[m_pattern[i]] = i;
        // }
        // idxQ[m_pattern[patternLength - 1]].push(patternLength - 1);
        // closeIdx[m_pattern[patternLength - 1]] = patternLength - 1;
        //
        // auto comp = [](const std::pair<int,double>& a, const std::pair<int,double>& b){return a.second > b.second;};
        // std::priority_queue<std::pair<int,double>,std::deque<std::pair<int,double>>, decltype(comp)> pq(comp);
        //
        // for(int i = patternLength - 1; i >= 0; --i){
        //     char target = m_pattern[i];
        //     idxQ[target].pop();
        //     double expectedShift = 0.0;
        //     for(int j = 0; j < 256; ++j){
        //         // if(j != target){
        //             // if(closeIdx[j]!=-1){
        //         expectedShift += (i - closeIdx[j]) * CharacterRatio[j]; //should be the same
        //             // }
        //             // else{
        //             //     expectedShift += (i + 1) * CharacterRatio[j];
        //             // }
        //         // }
        //     }
        //     double weights = expectedShift / CharacterRatio[target];
        //     if(!idxQ[target].empty()){
        //         closeIdx[target] = idxQ[target].top();
        //     }
        //     else{
        //         closeIdx[target] = -1;
        //     }
        //     pq.push(std::make_pair(i,weights));
        // }
        //
        // int i = 0;
        // while(!pq.empty()){
        //     auto cur = pq.top();
        //     pq.pop();
        //     order[i++] = cur.first;
        // }

        int patternLength = m_pattern.size();
        std::unordered_map<char,int> closeIdx;
        std::vector<std::pair<int,double>> exp(patternLength);
        order.resize(patternLength);
        for(int i = 0; i < 256; i++){
            preprocessTable[i] = patternLength;
        }
        for(int i = 0; i < patternLength - 1; ++i){
            preprocessTable[m_pattern[i]] = patternLength - 1 - i;
        }

        for(int i = 0; i < patternLength; ++i){
            double cumProb = 0.0;
            double expectedShift = 0.0;
            double base = (1 - CharacterRatio[m_pattern[i]]);
            for(auto it = closeIdx.begin(), at = closeIdx.end(); it != at; ++it){
                if(it->first != m_pattern[i]){
                    expectedShift += (i - it->second) * (CharacterRatio[it->first] / base);
                    cumProb += CharacterRatio[it->first] / base;
                }
            }
            expectedShift += (1 - cumProb) * (i + 1);
            expectedShift /= CharacterRatio[m_pattern[i]];
            closeIdx[m_pattern[i]] = i;
            exp[i] = std::make_pair(i,expectedShift);
        }

        auto comp = [](const std::pair<int,double>& a, const std::pair<int,double>& b){return a.second < b.second;};
        std::sort(exp.begin(),exp.end(),comp);

        for(int i = 0; i < patternLength; ++i){
            order[i] = exp[i].first;
        }

    }

    void probHorspoolTry::findOne(int& firstPosition){
        int skip = 0;
        int textLength = m_text.size();
        int patternLength = m_pattern.size();
        while(textLength - skip >= patternLength){
            int i = patternLength - 1;
            int j = skip + order[i];
            while(cmp(order[i],j)){
                if(i == 0){
                    firstPosition = skip;
                    return;
                }
                --i;
                j = skip + order[i];
            }
            skip = skip + preprocessTable[m_text[skip + patternLength - 1]];
        }
    }

    void probHorspoolTry::findAll(std::vector<int>& allPositions){
        int skip = 0;
        int textLength = m_text.size();
        int patternLength = m_pattern.size();
        while(textLength - skip >= patternLength){
            int i = patternLength - 1;
            int j = skip + order[i];
            while(cmp(order[i],j)){
                if(i == 0){
                    allPositions.push_back(skip);
                    break;
                }
                --i;
                j = skip + order[i];
            }
            skip = skip + preprocessTable[m_text[skip + patternLength - 1]];
        }
    }
}
