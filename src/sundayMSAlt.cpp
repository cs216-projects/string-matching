#include <sundayMSAlt.hpp>
#include <algorithm>

namespace smb{
    int SundayMSAlt::matchShift(int ploc, int lshift){
        int i,j;
        int patternLength = m_pattern.size();
        for(;lshift < patternLength; ++lshift){
            i = ploc;
            while(--i >= 0){
                if((j = (patternOrder[i].first - lshift)) < 0)
                    continue;
                if (patternOrder[i].second != m_pattern[j])
                    break;
            }
            if (i < 0)
                break;
        }
        return lshift;
    }

    void SundayMSAlt::preProcessing(){
        int patternLength = m_pattern.size();
        patternOrder.resize(patternLength);
        TD2.resize(patternLength);
        minShift.resize(patternLength);

        for(int i = 0; i < patternLength; ++i){
            int j = i - 1;
            for(; j >= 0; --j){
                if (m_pattern[i] == m_pattern[j])
                    break;
            }
            minShift[i] = i - j;
        }

        for(int i = 0; i < 256; ++i){
            TD1[i] = patternLength + 1;
        }
        for(int i = 0; i < patternLength; ++i){
            TD1[m_pattern[i]] = patternLength - i;
            patternOrder[i] = std::make_pair(i,m_pattern[i]);
        }

        auto comp = [this](const std::pair<int,char>& a, const std::pair<int,char>& b){
            if(this->minShift[a.first] == this->minShift[b.first]){
                return a.first > b.first;
            }
            else{
                return this->minShift[a.first] > this->minShift[b.first];
            }
        };

        std::sort(patternOrder.begin(),patternOrder.end(),comp);

        int lshift,i,ploc;
        TD2[0] = 1;
        lshift = 1;
        for(ploc = 1; ploc < patternLength; ++ploc){
            lshift = matchShift(ploc,lshift);
            TD2[ploc] = lshift;
        }
        for(ploc = 0; ploc < patternLength; ++ploc){
            lshift = TD2[ploc];
            while(lshift < patternLength){
                i = patternOrder[ploc].first - lshift;
                if (i < 0 || patternOrder[ploc].second != m_pattern[i])
                    break;
                ++lshift;
                lshift = matchShift(ploc,lshift);
            }
            TD2[ploc] = lshift;
        }
    }

    void SundayMSAlt::findOne(int& firstPosition){
        int i = 0;
        int j = 0;
        int n = m_text.size();
        int m = m_pattern.size();

        while(j <= n - m){
            i = 0;
            while(i < m){
                int z = patternOrder[i].first + j;
                if (!cmp(patternOrder[i].first,z))break;
                ++i;
            }
            if(i >= m){
                firstPosition = j;
                return;
            }
            j += std::max(TD2[i],TD1[m_text[j+m]]);
        }
    }

    void SundayMSAlt::findAll(std::vector<int>& allPositions){
        int i = 0;
        int j = 0;
        int n = m_text.size();
        int m = m_pattern.size();

        while(j <= n - m){
            i = 0;
            while(i < m){
                int z = patternOrder[i].first + j;
                if (!cmp(patternOrder[i].first,z))break;
                ++i;
            }
            if(i >= m){
                allPositions.push_back(j);
            }
            if(j == n - m)return;
            j += std::max(TD2[i],TD1[m_text[j+m]]);
        }
    }
}
