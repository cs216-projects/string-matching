#include <sundayMS.hpp>
#include <numeric>
#include <algorithm>

namespace smb {
// namespace for string-match benchmark

void SundayMS::preProcessing()
{
  int patternLength = m_pattern.size();
  // Build TD1
  for(int ch = 0; ch < 256; ch++)
  {
    TD1[ch] = patternLength + 1;
  }
  for(int pos = 0; pos < patternLength; pos++)
  {
    TD1[m_pattern[pos]] = patternLength - pos;
  }

  // Find distance to previous occurance of letter
  int prevOccurancePos[256];
  for (int ch = 0; ch < 256; ch++)
  {
    prevOccurancePos[ch] = -1;
  }
    
  std::vector<int> distanceToPrev;
  for (int pos = 0; pos < patternLength; pos++)
  {
    char ch = m_pattern[pos];
    distanceToPrev.push_back(pos - prevOccurancePos[ch]);
    prevOccurancePos[ch] = pos;
  }

  // Create ordering based on distance
  patternOrder.resize(patternLength);
  std::iota (patternOrder.begin(), patternOrder.end(), 0);

  struct Comparer{
    std::vector<int> dist;

    Comparer(std::vector<int>&d) { dist = d; }
    bool operator() (int a, int b) {
      if (dist[a] < dist[b])
        return true;
      else if (dist[a] > dist[b])
        return false;
      else
        return a > b; // tiebreak by prefering rightmost index
    }
  } comparer(distanceToPrev);

  std::sort(patternOrder.begin(), patternOrder.end(), comparer);

  // Initialize TD2
  TD2.resize(patternLength);
  int lshift = 1;
  for (int j = 0; j < patternLength; j++)
  {
    // Find smallest possible shift that could match
    for (int newShift = lshift; newShift <= patternLength; newShift++)
    {
      bool canMatch = true;
      for (int i = 0; canMatch && i < j; i++)
      {
        int pos = patternOrder[i];
        canMatch = canMatch && (pos < newShift || m_pattern[pos] == m_pattern[pos - newShift]);
      }
      int pos = patternOrder[j];
      canMatch = canMatch && (pos < newShift || m_pattern[pos] != m_pattern[pos - newShift]);

      if(canMatch)
      {
        lshift = newShift;
        break;
      }
    }

    TD2[j] = lshift;
  }

}

void SundayMS::findOne(int& firstPosition)
{
  int m = m_pattern.size();
  int n = m_text.size();
  int k = 0;

  while (k + m < n)
  {
    int j = 0;
    while (j < m && cmp(patternOrder[j], k + patternOrder[j])) 
    {
      j++;
    }
    if (j == m)
    {
      firstPosition = k;
      return;
    }
    int delta1 = TD1[ m_text[k + m] ];
    int delta2 = TD2[j];
    k += delta1 > delta2 ? delta1 : delta2;
  }
}

void SundayMS::findAll(std::vector<int>& allPositions)
{
  int m = m_pattern.size();
  int n = m_text.size();
  int k = 0;

  while (k + m < n)
  {
    int j = 0;
    while (j < m && cmp(patternOrder[j], k + patternOrder[j])) 
    {
      j++;
    }
    if (j == m)
    {
      allPositions.push_back(k);
      k++;
    }
    else
    {
      int delta1 = TD1[ m_text[k + m] ];
      int delta2 = TD2[j];
      k += delta1 > delta2 ? delta1 : delta2;
    }
  }
}

}// namespace smb
