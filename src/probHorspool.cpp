
#include <probHorspool.hpp>
#include <queue>
#include <deque>

namespace smb{
    void probHorspool::preProcessing(){
        std::vector<std::vector<int>> idxes(256, std::vector<int>());
        int patternLength = m_pattern.size();
        order.clear();
        order.resize(patternLength);
        for(int i = 0; i < 256; i++){
            preprocessTable[i] = patternLength;
        }
        for(int i = 0; i < patternLength - 1; ++i){
            preprocessTable[m_pattern[i]] = patternLength - 1 - i;
            idxes[m_pattern[i]].push_back(i);
        }
        idxes[m_pattern[patternLength - 1]].push_back(patternLength - 1);

        auto comp = [this](const int a, const int b){return this->CharacterRatio[a] < this->CharacterRatio[b];};
        std::priority_queue<int, std::deque<int>,  decltype( comp )> pq(comp);
        for(int i = 0; i < 256; ++i){
            if(idxes[i].size() > 0){
                pq.push(i);
            }
        }

        int i = 0;
        while(!pq.empty()){
            int idx = pq.top();
            pq.pop();
            for(int j : idxes[idx]){
                order[i++] = j;
            }
        }
    }

    void probHorspool::findOne(int& firstPosition){
        int skip = 0;
        int textLength = m_text.size();
        int patternLength = m_pattern.size();
        while(textLength - skip >= patternLength){
            int i = patternLength - 1;
            int j = skip + order[i];
            while(cmp(order[i],j)){
                if(i == 0){
                    firstPosition = skip;
                    return;
                }
                --i;
                j = skip + order[i];
            }
            skip = skip + preprocessTable[m_text[skip + patternLength - 1]];
        }
    }

    void probHorspool::findAll(std::vector<int>& allPositions){
        int skip = 0;
        int textLength = m_text.size();
        int patternLength = m_pattern.size();
        while(textLength - skip >= patternLength){
            int i = patternLength - 1;
            int j = skip + order[i];
            while(cmp(order[i],j)){
                if(i == 0){
                    allPositions.push_back(skip);
                    break;
                }
                --i;
                j = skip + order[i];
            }
            skip = skip + preprocessTable[m_text[skip + patternLength - 1]];
        }
    }


}
