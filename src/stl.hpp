/**
 * @file   stl.hpp
 * @author Yanbiao Li <lybmath@cs.ucla.edu>
 * @date   Sun May 20 21:42:57 2018
 * 
 * @brief  a wapper of STL string matching algorithm
 * 
 */

#include <algo-base.hpp>

namespace smb {
// namespace for string-match benchmark

class STL : public StringMatchingAlgorithm
{
protected:
  /** 
   * pre-processing before start string matching
   * 
   */
  void preProcessing();

  /** 
   * find the first appearance of the pattern in the text
   * 
   * @param firstPosition capture out the first appearing position (start from 0)
   */
  void findOne(int& firstPosition);

  /** 
   * find all appearances of the pattern in the text
   * 
   * @param allPositions capture out all appearing positions
   */
  void findAll(std::vector<int>& allPositions);  
};

}// namespace smb
