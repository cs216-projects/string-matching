
#include <sundayQS.hpp>


namespace smb{
    void sundayQS::preProcessing(){
        int patternLength = m_pattern.size();
        for(int i = 0; i < 256; i++){
            preprocessTable[i] = patternLength + 1;
        }
        for(int i = 0; i < patternLength; i++){
            preprocessTable[m_pattern[i]] = patternLength - i;
        }
    }

    void sundayQS::findOne(int& firstPosition){
        int skip = 0;
        int textLength = m_text.size();
        int patternLength = m_pattern.size();
        while(textLength - skip >= patternLength){
            int i = patternLength - 1;
            int j = skip + i;
            while(cmp(i,j)){
                if(i == 0){
                    firstPosition = skip;
                    return;
                }
                --i;
                --j;
            }
            if(skip + patternLength < textLength)skip = skip + preprocessTable[m_text[skip + patternLength]];
            else break;
        }
    }

    void sundayQS::findAll(std::vector<int>& allPositions){
        int skip = 0;
        int textLength = m_text.size();
        int patternLength = m_pattern.size();
        while(textLength - skip >= patternLength){
            int i = patternLength - 1;
            int j = skip + i;
            while(cmp(i,j)){
                if(i == 0){
                    allPositions.push_back(skip);
                    break;
                }
                --i;
                --j;
            }
            if(skip + patternLength < textLength)skip = skip + preprocessTable[m_text[skip + patternLength]];
            else break;
        }
    }
}
