#! /bin/bash

#Usage: ./analyze.sh [pattern|text] StartValue EndValue StepSize [DNA|Corpus|Alpha] [total|match|comp]

param=$1
start=$2
end=$3
stepsize=$4
datatype=$5
ylabel=$6

echo "Selected Data $datatype"

#ylabel mapping
if [ "$ylabel" == "total" ]; then
    ylabel="Total Time (us)"
    lineNum=26
elif [ "$ylabel" == "match" ]; then
    ylabel="Matching Time (us)"
    lineNum=22
elif [ "$ylabel" == "comp" ]; then
    ylabel="Comparison"
    lineNum=5
fi

pArr='['
declare -a testArr
index=0
while [ $start -le $end ]; do
    testArr[index]=$start
    pArr=$pArr$start','
    let start=$start+$stepsize
    let index=index+1
done
echo "Test Param $param"
echo "Testing Range is ${testArr[@]}"
pArr=$(echo $pArr | sed "s/\(.*\).$/\1]/")
if [ "$param" == "pattern" ]; then
    title="Time VS. Pattern Length with Text Length 50000 on $datatype"    
elif [ "$param" == "text" ]; then
    title="Time VS. Text Length with Pattern Length 50 on $datatype"  
else
    echo "unknown parameter $param"
fi

benchdict='{'
algs=$(cat output.txt | tr '[:,]' ' ' | awk '{print $1}' | sort | uniq )
for alg in $algs
do
    time=$(grep $alg':' output.txt | tr ',' ' ' | awk -v line="$lineNum" '{print $line}')
    benchdict="$benchdict'$alg':["
    for t in $time
    do
        benchdict="$benchdict$t,"
    done
    benchdict=$(echo $benchdict | sed "s/\(.*\).$/\1],/")
done
benchdict=$(echo $benchdict | sed "s/\(.*\).$/\1}/")
python3 plot.py -p $param -v $pArr -r $benchdict -t "$title" -y "$ylabel"
