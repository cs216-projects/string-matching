#! /bin/bash

#Usage: ./experiment.sh [pattern|text] StartValue EndValue StepSize [DNA|Corpus|Alpha]

if [ -f output.txt ]; then
    rm -f output.txt
fi
param=$1
start=$2
end=$3
stepsize=$4
datatype=$5

# datatype mapping
if [ "$datatype" == "DNA" ]; then
    dataflag="-d"
elif [ "$datatype" == "Corpus" ]; then
    dataflag="-c"
else
    dataflag="-a"
fi
echo "Selected Data $datatype"

declare -a testArr
index=0
while [ $start -le $end ]; do
    testArr[index]=$start
    let start=$start+$stepsize
    let index=index+1
done

echo "Test Param $param"
echo "Testing Range is ${testArr[@]}"
if [ "$param" == "pattern" ]; then
    for plen in ${testArr[@]}
    do
        $(cd ../ && ./build/StringBench.app $dataflag -p $plen -t 50000 >> ./analyze/output.txt 2>&1)
        echo "" >> output.txt
    done
elif [ "$param" == "text" ]; then 
    for plen in ${testArr[@]}
    do
        $(cd ../ && ./build/StringBench.app $dataflag -p 50 -t $plen >> ./analyze/output.txt 2>&1)
        echo "" >> output.txt
    done
else
    echo "unknown parameter $param"
fi

echo "Finished test runs. Test results are in output.txt."