import matplotlib.pyplot as plt
import argparse
import numpy as np
from cycler import cycler

parser = argparse.ArgumentParser()
parser.add_argument('-p',dest='param',type=str,required=True)
parser.add_argument('-v',dest='pvalue',type=str,required=True)
parser.add_argument('-r',dest='result',type=str,required=True)
parser.add_argument('-t',dest='title',type=str,required=True)
parser.add_argument('-y',dest='ylabel',type=str,required=True)
arg = parser.parse_args()

paramName = arg.param
paramValue = np.array(eval(arg.pvalue))
testResult = eval(arg.result)

ax = plt.subplot(111)
ax.set_title(arg.title)
num_colors = len(testResult)
cm = plt.get_cmap('nipy_spectral',num_colors)
ax.set_prop_cycle(cycler('color',[cm(i) for i in range(num_colors)]))
for k,v in testResult.items():
    ax.plot(paramValue,np.array(v),'.-',label=k)
plt.xlabel(paramName)
plt.ylabel(arg.ylabel)
box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax.legend(loc='upper center',prop={'size': 6}, bbox_to_anchor=(0.5, -0.15), fancybox=True, shadow=True, ncol=5)
plt.show()

