import sys
import ast
# import pandas
# import csv
# import pyplot

if __name__ == '__main__' :
    try :
        dataResults = sys.argv[1]
        fileName = sys.argv[2]

        # create the CSV file
        with open(str.format('benchmark-results/results/{0}', fileName), 'w+') as file:
            file.write(dataResults)

        # read from CSV file and plot
        # with open(str.format('benchmark-results/results/{0}', fileName), 'r') as csvFile:
        #     df = pandas.read_csv(csvFile)
    except Exception as e:
        print(str.format("PYTHON SCRIPT ERROR: {0}", e))